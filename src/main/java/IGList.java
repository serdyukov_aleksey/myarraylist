public interface IGList <T> {

    void clear();
    int size();
    Object get(int index);
    boolean add(T t);
    boolean add(int index, T t);
    int remove(T t);
    int removeByIndex(int index);
    boolean contains(T t);
    boolean set(int index, T t);
    void print(); //=> выводит в консоль массив в квадратных скобках и через запятую
    Object[] toArray();// => приводит данные к массиву, в случае с AList ничего сложного, у нас и так массив
    Object[] subList(int fromIndex, int toIndex);
    boolean removeAll(T[] ar);
    boolean retainAll(T[] ar);

}
