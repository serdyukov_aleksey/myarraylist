public interface IList {

    void clear();
    int size();
    int get(int index);
    boolean add(int number);
    boolean add(int index, int number);
    int remove(int number);
    int removeByIndex(int index);
    boolean contains(int number);
    boolean set(int index, int number);
    void print(); //=> выводит в консоль массив в квадратных скобках и через запятую
    int[] toArray();// => приводит данные к массиву, в случае с AList ничего сложного, у нас и так массив
    int[] subList(int fromIndex, int toIndex);
    boolean removeAll(int[] ar);
    boolean retainAll(int[] ar);

}
