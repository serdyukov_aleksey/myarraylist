public class MyArrayList implements IList{

    private static final int DEFAULT_CAPACITY = 10;
    private int[] array;
    private int size;
    private int count = 0;
    private int[] tempArr;
    private int[]tempArr2;

    public MyArrayList() {
        reLoad();
    }

    public MyArrayList(int capacity) {
        if (capacity > 0) {
            array = new int[capacity];
        }
    }

    public MyArrayList(int[] array) {
        this.array = array;
    }

    public int getLength() {
        return this.array.length;
    }

    @Override
    public void clear() {
        size = 0;
        array = new int [DEFAULT_CAPACITY];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int get(int index) {
        return array[index];
    }

    @Override
    public boolean add(int number) {
        array[size] = number;
        size++;
        if (size == array.length) {
            sizeArr();
        }
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        if(index <= size) {
            if (size == array.length-1) {
                sizeArr();
            }
            tempArr = subList(index-1, size);
            for (int i = index; i <= size; i++) {
                array[i] = tempArr[i - index];
            }
            array[index] = number;
            size++;
            return true;
        }
        return false;
    }

    @Override
    public int remove(int number) {
        for (Integer i : array) {
            if(number == i){
                continue;
            }
            array[count++] = i;
        }
        size--;
        count = 0;
        return 1;
    }

    @Override
    public int removeByIndex(int index) {
        remove(array[index]);
        return 1;
    }

    @Override
    public boolean contains(int number) {
        for (Integer i : array) {
            if (number == i){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, int number) {
        array[index] = number;
        return true;
    }

    @Override
    public void print() {
        for (int i = 0; i < size; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    @Override
    public int[] toArray() {
        int[] toarr = new int[size];
        for (int i = 0; i < size; i++){
            toarr[i] = array[i];
        }
        return toarr;
    }

    @Override
    public int[] subList(int fromIndex, int toIndex) {
        tempArr = new int[toIndex - fromIndex];
        for (int i = fromIndex; i < toIndex; i++){
            tempArr[i - fromIndex] = array[i];
        }
        return tempArr;
    }

    @Override
    public boolean removeAll(int[] arr) {
        for (int i : arr) {
            if (contains(i)){
                remove(i);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(int[] ar) {
        tempArr2 = new int[ar.length];
        for (Integer i : ar){
            if (contains(i)){
                tempArr2[count++] = i;
            }
        }
        size = 0;
        reLoad();
        for (int i = 0; i < count; i++){
            add(tempArr2[i]);
        }
        count = 0;
        return true;
    }

    private void sizeArr(){
        int index = (int)(size * 0.5) + size;
        tempArr = array;
        array = new int[index];
        for (int i = 0; i < tempArr.length; i++){
            array[i] = tempArr[i];
        }
    }

    private void reLoad(){
        array = new int[DEFAULT_CAPACITY];
    }

    public int[] arrList(){
        return array;
    }
}
