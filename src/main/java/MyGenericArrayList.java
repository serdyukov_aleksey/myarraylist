public class MyGenericArrayList<T> implements IGList{

    private static final int DEFAULT_CAPACITY = 10;
    private Object[] array;
    private int size;
    private int count = 0;
    private Object[] tempArr;
    private Object[]tempArr2;

    public MyGenericArrayList() {
        reLoad();
    }

    public MyGenericArrayList(int capacity) {
        if (capacity > 0) {
            array = new Object[capacity];
        }
    }

    public MyGenericArrayList(Object[] array) {
        this.array = array;
    }

    public int getLength() {
        return this.array.length;
    }

    @Override
    public void clear() {
        size = 0;
        array = new Object [DEFAULT_CAPACITY];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object get(int index) {
        return array[index];
    }

    @Override
    public boolean add(Object e) {
        array[size] = e;
        size++;
        if (size == array.length) {
            sizeArr();
        }
        return true;
    }

    @Override
    public boolean add(int index, Object e) {
        if(index <= size) {
            if (size == array.length-1) {
                sizeArr();
            }
            tempArr = subList(index-1, size);
            for (int i = index; i <= size; i++) {
                array[i] = tempArr[i - index];
            }
            array[index] = e;
            size++;
            return true;
        }
        return false;
    }

    @Override
    public int remove(Object e) {
        for (Object elem : array) {
            if(e.equals(elem)){
                continue;
            }
            array[count++] = elem;
        }
        size--;
        count = 0;
        return 1;
    }

    @Override
    public int removeByIndex(int index) {
        remove(array[index]);
        return 1;
    }

    @Override
    public boolean contains(Object e) {
        for (Object elem : array) {
            if (e.equals(elem)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, Object e) {
        array[index] = e;
        return true;
    }

    @Override
    public void print() {
        for (int i = 0; i < size; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    @Override
    public Object[] toArray() {
        Object[] toarr = new Object[size];
        for (int i = 0; i < size; i++){
            toarr[i] = array[i];
        }
        return toarr;
    }

    @Override
    public Object[] subList(int fromIndex, int toIndex) {
        tempArr = new Object[toIndex - fromIndex];
        for (int i = fromIndex; i < toIndex; i++){
            tempArr[i - fromIndex] = array[i];
        }
        return tempArr;
    }

    @Override
    public boolean removeAll(Object[] arr) {
        for (Object elem : arr) {
            if (contains(elem)){
                remove(elem);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Object[] ar) {
        tempArr2 = new Object[ar.length];
        for (Object elem : ar){
            if (contains(elem)){
                tempArr2[count++] = elem;
            }
        }
        size = 0;
        reLoad();
        for (int i = 0; i < count; i++){
            add(tempArr2[i]);
        }
        count = 0;
        return true;
    }

    private void sizeArr(){
        int index = (int)(size * 0.5) + size;
        tempArr = array;
        array = new Object[index];
        for (int i = 0; i < tempArr.length; i++){
            array[i] = tempArr[i];
        }
    }

    private void reLoad(){
        array = new Object[DEFAULT_CAPACITY];
    }

    public Object[] arrList(){
        return array;
    }
}
