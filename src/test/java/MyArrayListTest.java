import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {

    private final int[] TEST_ARR = {2,3,5,8,9};
    MyArrayList myArrayList;


    MyArrayListTest() {
        myArrayList = new MyArrayList();
    }
    @BeforeEach
    public void initAListI(){
        myArrayList.add(2);
        myArrayList.add(4);
        myArrayList.add(6);
        myArrayList.add(8);
        myArrayList.add(10);
        myArrayList.add(12);
        myArrayList.add(14);
        myArrayList.add(16);
        myArrayList.add(18);
        myArrayList.add(20);
    }

    @Test
    public void test1GetLength(){
        int exp = 15;
        int act = myArrayList.getLength();
        assertEquals(exp, act);
    }

    @Test
    public void test2GetLength(){
        int exp = 22;
        myArrayList.add(21);
        myArrayList.add(22);
        myArrayList.add(23);
        myArrayList.add(24);
        myArrayList.add(25);
        int act = myArrayList.getLength();
        assertEquals(exp, act);
    }

    @Test
    public void testClear(){
        int[] exp = {0,0,0,0,0,0,0,0,0,0};
        myArrayList.clear();
        int[] act = myArrayList.arrList();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSize(){
        int exp = 10;
        int act = myArrayList.size();
        assertEquals(exp, act);
    }

    @Test
    public void testGet(){
        int exp = 12;
        int act = myArrayList.get(5);
        assertEquals(exp, act);
    }

    @Test
    public void testAdd(){
        int exp = 22;
        myArrayList.add(22);
        int act = myArrayList.get(10);
        assertEquals(exp, act);
    }

    @Test
    public void testAddIndex(){
        myArrayList.add(2, 24);
        int exp = 24;
        int[] exp2 = {2,4,24,6,8,10,12,14,16,18,20};
        int act = myArrayList.get(2);
        int[] act2 = myArrayList.toArray();
        assertEquals(exp, act);
        assertArrayEquals(exp2, act2);
    }

    @Test
    public void testRemove(){
        myArrayList.remove(12);
        int[] exp = {2,4,6,8,10,14,16,18,20};
        int[] act = myArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testRemoveByIndex(){
        myArrayList.removeByIndex(8);
        int[] exp = {2,4,6,8,10,12,14,16,20};
        int[] act = myArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testContains(){
        boolean act1 = myArrayList.contains(6);
        boolean act2 = myArrayList.contains(5);
        assertTrue(act1);
        assertFalse(act2);
    }

    @Test
    public void testSet(){
        myArrayList.set(3,9);
        int[] exp = {2,4,6,9,10,12,14,16,18,20};
        int[] act = myArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArray(){
        int[] exp = {2,4,6,8,10,12,14,16,18,20};
        int[] act = myArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSubList(){
        int[] exp = {8,10,12,14};
        int[] act = myArrayList.subList(3,7);
        assertArrayEquals(exp, act);
    }

    @Test
    public void testRemoveAll(){
        myArrayList.removeAll(TEST_ARR);
        int[] exp = {4,6,10,12,14,16,18,20};
        int[] act = myArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testRetainAll(){
        myArrayList.retainAll(TEST_ARR);
        int[] exp = {2,8};
        int[] act = myArrayList.toArray();
        assertArrayEquals(exp, act);
    }
}