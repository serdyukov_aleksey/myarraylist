import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyGenericArrayListTest {

    private final String[] TEST_ARR = {"2","3","5","8","9"};
    MyGenericArrayList<String> myGenericArrayList;


    MyGenericArrayListTest() {
        myGenericArrayList = new MyGenericArrayList<String>();
        List<String> alist = new ArrayList<String>();
    }
    @BeforeEach
    public void initAListI(){
        myGenericArrayList.add("2");
        myGenericArrayList.add("4");
        myGenericArrayList.add("6");
        myGenericArrayList.add("8");
        myGenericArrayList.add("10");
        myGenericArrayList.add("eee");
        myGenericArrayList.add("14");
        myGenericArrayList.add("16");
        myGenericArrayList.add("18");
        myGenericArrayList.add("20");
    }

    @Test
    public void test1GetLength(){
        int exp = 15;
        int act = myGenericArrayList.getLength();
        assertEquals(exp, act);
    }

    @Test
    public void test2GetLength(){
        int exp = 22;
        myGenericArrayList.add("21");
        myGenericArrayList.add("22");
        myGenericArrayList.add("23");
        myGenericArrayList.add("24");
        myGenericArrayList.add("25");
        int act = myGenericArrayList.getLength();
        assertEquals(exp, act);
    }

    @Test
    public void testClear(){
        String[] exp = {null,null,null,null,null,null,null,null,null,null};
        myGenericArrayList.clear();
        String [] act = (String[]) myGenericArrayList.arrList();
        assertArrayEquals(exp, act);
    }

//    @Test
//    public void testSize(){
//        int exp = 10;
//        int act = myArrayList.size();
//        assertEquals(exp, act);
//    }
//
    @Test
    public void testGet(){
        String exp = "eee";
        String act = (String) myGenericArrayList.get(5);
        assertEquals(exp, act);
    }

    @Test
    public void testAdd(){
        String exp = "22";
        myGenericArrayList.add("22");
        String act = (String) myGenericArrayList.get(10);
        assertEquals(exp, act);
    }

    @Test
    public void testAddIndex(){
        myGenericArrayList.add(2, "sss");
        String exp = "sss";
        String[] exp2 = {"2","4","sss","6","8","10","eee","14","16","18","20"};
        String act = (String) myGenericArrayList.get(2);
        Object[] act2 =  myGenericArrayList.toArray();
        assertEquals(exp, act);
        assertArrayEquals(exp2, act2);
    }

    @Test
    public void testRemove(){
        myGenericArrayList.remove("eee");
        String[] exp = {"2","4","6","8","10","14","16","18","20"};
        Object[] act = myGenericArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testRemoveByIndex(){
        myGenericArrayList.removeByIndex(8);
        String[] exp = {"2","4","6","8","10","eee","14","16","20"};
        Object[] act = myGenericArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testContains(){
        boolean act1 = myGenericArrayList.contains("6");
        boolean act2 = myGenericArrayList.contains("5");
        assertTrue(act1);
        assertFalse(act2);
    }

    @Test
    public void testSet(){
        myGenericArrayList.set(3,"9");
        String[] exp = {"2","4","6","9","10","eee","14","16","18","20"};
        Object[] act = myGenericArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArray(){
        String[] exp = {"2","4","6","8","10","eee","14","16","18","20"};
        Object[] act =  myGenericArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSubList(){
        String[] exp = {"8","10","eee","14"};
        Object[] act = myGenericArrayList.subList(3,7);
        assertArrayEquals(exp, act);
    }

    @Test
    public void testRemoveAll(){
        myGenericArrayList.removeAll(TEST_ARR);
        String[] exp = {"4","6","10","eee","14","16","18","20"};
        Object[] act = myGenericArrayList.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void testRetainAll(){
        myGenericArrayList.retainAll(TEST_ARR);
        String[] exp = {"2","8"};
        Object[] act = myGenericArrayList.toArray();
        assertArrayEquals(exp, act);
    }
}